﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int left = 0;
            int start = 0;
            int currentItem = 0;
            string toFind = textBox1.Text.Trim();
            List<string> searchable = new List<string>();
            List<string> captionSearch = new List<string>();

            if (checkBox1.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/vshehm/ochnaja/");
                captionSearch.Add("ВШЭМ");
            }
            if (checkBox2.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/fti/ochnaja/");
                captionSearch.Add("ФТИ");
            }
            if (checkBox3.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/uralehnin/ochnaja/");
                captionSearch.Add("УралЭНИН");
            }
            if (checkBox4.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/ivtob/ochnaja/");
                captionSearch.Add(checkBox4.Text);
            }
            if (checkBox5.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/igni/ochnaja/");
                captionSearch.Add(checkBox5.Text);
            }
            if (checkBox6.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/imkn/ochnaja/");
                captionSearch.Add(checkBox6.Text);
            }
            if (checkBox7.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/ien/ochnaja/");
                captionSearch.Add(checkBox7.Text);
            }
            if (checkBox8.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/immt/ochnaja/");
                captionSearch.Add(checkBox8.Text);
            }
            if (checkBox9.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/irit-rtf/ochnaja/");
                captionSearch.Add(checkBox9.Text);
            }
            if (checkBox10.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/ispn/ochnaja/");
                captionSearch.Add(checkBox10.Text);
            }
            if (checkBox11.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/iup/ochnaja/");
                captionSearch.Add(checkBox11.Text);
            }
            if (checkBox12.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/ifksu/ochnaja/");
                captionSearch.Add(checkBox12.Text);
            }
            if (checkBox13.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/info/ochnaja/");
                captionSearch.Add(checkBox13.Text);
            }
            if (checkBox14.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/mmi/ochnaja/");
                captionSearch.Add(checkBox14.Text);
            }
            if (checkBox15.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/sti/ochnaja/");
                captionSearch.Add(checkBox15.Text);
            }
            if (checkBox16.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/khti/ochnaja/");
                captionSearch.Add(checkBox16.Text);
            }
            if (checkBox17.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/ioit/ochnaja/");
                captionSearch.Add(checkBox17.Text);
            }
            if (checkBox18.Checked)
            {
                searchable.Add("http://urfu.ru/applicant/supply/enrollment/rating-2013/fuo/ochno-zaochnaja/");
                captionSearch.Add(checkBox18.Text);
            }

            WebClient wc = new WebClient();
            wc.Encoding = System.Text.Encoding.UTF8;
            if (searchable.Count == 0)
            {
                return;
            }
            int cenaDelenia = 100 / (searchable.Count * 2);
            
            progressBar1.Visible = true;
            
            foreach (String url in searchable)
            {
                Uri ui = new Uri(url);
                currentItem++;
                //wc.DownloadFile(ui, "C://site.html");
                progressBar1.Value += cenaDelenia;
                string str = wc.DownloadString(ui);
                progressBar1.Value += cenaDelenia;
                //MessageBox.Show(str);
                int a = 0;
                start = 0;
                int cnt3 = 0;//>
                while (a != -1 || str.Length > 100)
                {
                    str = str.Substring(start);
                    a = str.IndexOf(toFind);
                    int startA = a;
                    if (a == -1)
                        break;
                    cnt3 = 0;
                    while (cnt3 < 3)
                    {
                        if (str[a] == '>')
                            cnt3++;
                        a--;
                    }
                    a++;
                    left = a;
                    cnt3 = 0;
                    while (str[a] != '<')
                    {
                        cnt3++;
                        a++;
                    }
                    string t = str.Substring(left + 1, cnt3 - 1);
                    MessageBox.Show(captionSearch[currentItem-1] + ", Место в рейтинге: " + t);
                    start = startA + toFind.Length;
                }

                //StreamWriter Saver = new StreamWriter(File.Open("123" + ".html", FileMode.OpenOrCreate, FileAccess.Write), Encoding.UTF8);
                //Saver.WriteLine(str);
                //Saver.Close();
            }
            progressBar1.Visible = false;
            progressBar1.Value = 0;
            
        }
    }
}
